﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using System.Collections;

namespace HR
{
    class Employee
    {
        public int Employee_Num { get; set;}
        public string First_Name { get; set;}
        public string Last_Name { get; set; }
        public DateTime Date_Start { get; set; }
        public DateTime Stop_Work { get; set; }
        public int Id { get; set; }
        public string Employee_Type { get; set; }
        public DateTime Employee_Birthday { get; set; }
        public int Temporary_Or_No { get; set; }
        public string English_First_Name { get; set; }
        public string English_Last_Name { get; set; }
        public int seniority { get; set; }
        public bool Status { get; set; }
        public int Unit { get; set; }

        public DateTime Date_Start2 { get; set; }//הוספתי בשביל סינונים של תאריכי קליטה

        public Employee()
        {

        }

        /// <summary>
        /// בנאי עבור סינון תאריכי קליטה
        /// </summary>
        public Employee(DateTime date_start,DateTime date_start2)
        {
            Date_Start = date_start;
            Date_Start2 = date_start2;
        }

        public DataTable GetData(bool ShowAllEmployees,bool Filter_Date_Start)
        {
            DBService DBS = new DBService();
            DataTable dataTable = new DataTable();
            string Employee_Table;
            if (ShowAllEmployees == false)
            {
                 Employee_Table = $@"SELECT  right(trim(T.OVED),5) as Number ,L.PRATI as FirstName ,L.FAMILY as LastName,T.TEUDTZHUI as Id
                                    ,T.ADRESS,L.AVODA as DateStart,H.HOFIL3 as TypeTime, right(trim(L.MAHLAKA),3) as Unit,T.BIRTHDAY, H.HOMACH as EFirstName,H.HOMACTP as ELastName ,B.CDESC as UnitName                                    FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED left join hsali.hovl02 as H on T.OVED=H.HOOVD left join  BPCSFV30. CDPL01  as B on right(trim(L.MAHLAKA),2)=B.CDEPT
                                    WHERE T.mifal='01' and (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129') and substring(T.kpa,7,6 )= '000000' and H.HOMIF in ('01', '09')";
            }
            else
            {
                Employee_Table = $@"SELECT  right(trim(T.OVED),5) as Number , name1||name2||name3||name4||name5||name6 as FirstName,                                      fmly1||fmly2||fmly3||fmly4||fmly5||fmly6||fmly7||fmly8||fmly9||fmly10 as LastName,T.TEUDTZHUI as Id,
                                     T.ADRESS,L.AVODA as DateStart, H.HOFIL3 as TypeTime,right(trim(L.MAHLAKA),2) as Unit,T.BIRTHDAY,case when right(trim(T.KPA),6) = '000000' then '' else right(trim(T.KPA),6) end left                                    , H.HOMACH as EFirstName,H.HOMACTP as ELastName ,B.CDESC as UnitName
                                     FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED left join hsali.hovl02 as H on T.OVED=H.HOOVD left join  BPCSFV30. CDPL01  as B on right(trim(L.MAHLAKA),2)=B.CDEPT
                                     WHERE   (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129')  and (substring(T.kpa,11,2)  > '08' and (substring(T.kpa,11,2) < '80') or substring(T.kpa,7,6 )= '000000') and             	                     H.HOMIF in ('01', '09')";
            }
           
            try
            {               
                dataTable = DBS.executeSelectQueryNoParam(Employee_Table);
                ChangeDateFormat(dataTable,ShowAllEmployees);
                if (Filter_Date_Start) dataTable= FilterDateTable(dataTable);
            }
            catch (Exception EX)
            {
                MessageBox.Show(EX.Message);
            }
 
            return dataTable;
        }

    

        private void ChangeDateFormat(DataTable dataTable,bool ShowAllEmployees)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i]["BIRTHDAY"].ToString() != "000000" && dataTable.Rows[i]["BIRTHDAY"].ToString()!="") 
                    dataTable.Rows[i]["BIRTHDAY"] = DateTime.ParseExact(dataTable.Rows[i]["BIRTHDAY"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                if (dataTable.Rows[i]["DateStart"].ToString() != "000000" && dataTable.Rows[i]["DateStart"].ToString()!="")
                    dataTable.Rows[i]["DateStart"] = DateTime.ParseExact(dataTable.Rows[i]["DateStart"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                if(ShowAllEmployees==true)
                {
                    if (dataTable.Rows[i]["Left"].ToString() != "000000" && dataTable.Rows[i]["Left"].ToString()!="")
                        dataTable.Rows[i]["Left"] = DateTime.ParseExact(dataTable.Rows[i]["Left"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                }
                                      
            }
        }

        private DataTable FilterDateTable(DataTable dataTable)
        {
           
            DataTable Filter_Date_Table = new DataTable();
           
            for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
            {
                if (dataTable.Rows[i]["DateStart"].ToString() != "000000" && dataTable.Rows[i]["DateStart"].ToString() != "")
                {
                    DataRow dr = dataTable.Rows[i];
                    if (DateTime.Parse(dataTable.Rows[i]["DateStart"].ToString()) < Date_Start2 && DateTime.Parse(dataTable.Rows[i]["DateStart"].ToString()) > Date_Start) continue; 
                    else    dr.Delete();
                }
                else { dataTable.Rows.Remove(dataTable.Rows[i]); }
            }
            dataTable.AcceptChanges();


            //DateTime t = new DateTime(2018, 10, 01);
            //ArrayList Filt = new ArrayList();
            //רשימת שורות שצריך למחוק
            //for (int i = 0; i < dataTable.Rows.Count; i++)
            //{
            //    if (dataTable.Rows[i]["DateStart"].ToString() != "000000" && dataTable.Rows[i]["DateStart"].ToString() != "")
            //    {
            //        if (DateTime.Parse(dataTable.Rows[i]["DateStart"].ToString()) < t)
            //            Filt.Add(i);

            //    }
            //    //else { dataTable.Rows.Remove(dataTable.Rows[i]); }
            //}
            //for (int j = 0; j < Filt.Count; j++)
            //{
            //    dataTable.Rows.Remove(dataTable.Rows[int.Parse(Filt[j].ToString())]);
            //    Filt[j + 1] = int.Parse(Filt[j + 1].ToString()) - 1;
            //}

            Filter_Date_Table = dataTable;
            return Filter_Date_Table;
    }


        public DataTable TableAfterSearch(bool ShowAllEmployees,int Select_Search_Field,string TextSearch)
        {
            DBService DBS = new DBService();
            DataTable dataTable = new DataTable();
            string Employee_Result = $@"SELECT right(trim(T.OVED),5) as Number ,L.PRATI as FirstName ,L.FAMILY as LastName,
                                          T.TEUDTZHUI as Id,T.ADRESS,L.AVODA as DateStart, H.HOFIL3 as TypeTime, right(trim(L.MAHLAKA),2) as Unit,T.BIRTHDAY, H.HOMACH as EFirstName,H.HOMACTP as ELastName ,B.CDESC as UnitName                                          FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED left join hsali.hovl02 as H on T.OVED=H.HOOVD left join  BPCSFV30. CDPL01  as B on right(trim(L.MAHLAKA),2)=B.CDEPT
                                           WHERE T.mifal='01' and (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129') and substring(T.kpa,7,6 )= '000000' and H.HOMIF in ('01', '09') and ";
            string All_Employee_Result = $@"SELECT  right(trim(T.OVED),5) as Number , name1||name2||name3||name4||name5||name6 as FirstName,                                          fmly1||fmly2||fmly3||fmly4||fmly5||fmly6||fmly7||fmly8||fmly9||fmly10 as LastName,T.TEUDTZHUI as Id,T.ADRESS,
                                         L.AVODA as DateStart, H.HOFIL3 as TypeTime, right(trim(L.MAHLAKA),2) as Unit,T.BIRTHDAY,case when right(trim(T.KPA),6) = '000000' then '' else right(trim(T.KPA),6) end left, H.HOMACH as EFirstName,H.HOMACTP as ELastName ,B.CDESC as UnitName                                         FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED left join hsali.hovl02 as H on T.OVED=H.HOOVD left join  BPCSFV30. CDPL01  as B on right(trim(L.MAHLAKA),2)=B.CDEPT
                                         WHERE   (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129')  and (substring(T.kpa,11,2)  > '08' and (substring(T.kpa,11,2) < '80') or substring(T.kpa,7,6 )= '000000') and             	                         H.HOMIF in ('01', '09') ";
            bool Which_Str_Changed=false;//Employee_Result-false,All_Employee_Result=true
            switch (Select_Search_Field)
            {

                case 0:

                    if (ShowAllEmployees == false) Employee_Result += $@"right(trim(T.OVED),5)={TextSearch} ";
                    if (ShowAllEmployees == true) { All_Employee_Result += $@"right(trim(T.OVED),5) ={TextSearch}"; Which_Str_Changed = true; }
                       
                   
                    break;

                case 1:

                    if (ShowAllEmployees == false) Employee_Result += $@"trim(L.PRATI) ='{TextSearch}'";
                    if (ShowAllEmployees == true) { All_Employee_Result += $@"trim(name1||name2||name3||name4||name5||name6)='{TextSearch}'"; Which_Str_Changed = true; }

                        break;

                case 2:

                    if (ShowAllEmployees == false) Employee_Result += $@"T.TEUDTZHUI={TextSearch}";
                    if (ShowAllEmployees == true) { All_Employee_Result += $@"T.TEUDTZHUI={TextSearch}"; Which_Str_Changed = true; }

                        break;

                case 3:

                    if (ShowAllEmployees == false) Employee_Result += $@"right(trim(L.MAHLAKA),3)='{TextSearch}'  group by T.OVED  ,L.PRATI  ,L.FAMILY ,   T.ADRESS ,     T.TEUDTZHUI ,L.AVODA ,  H.HOFIL3 , L.MAHLAKA ,T.BIRTHDAY ,H.HOMACH,H.HOMACTP,B.CDESC";
                    if (ShowAllEmployees == true) { All_Employee_Result += $@"right(trim(L.MAHLAKA),3)='{TextSearch}'  group by T.OVED  ,L.PRATI  ,L.FAMILY ,   T.ADRESS ,     T.TEUDTZHUI ,L.AVODA ,  H.HOFIL3 , L.MAHLAKA ,T.BIRTHDAY ,H.HOMACH,H.HOMACTP,B.CDESC; "; Which_Str_Changed = true; }///לטפל בזה

                    break;

            }
            if (Which_Str_Changed) dataTable = DBS.executeSelectQueryNoParam(All_Employee_Result);
            else dataTable = DBS.executeSelectQueryNoParam(Employee_Result);
            ChangeDateFormat(dataTable, ShowAllEmployees);
            return dataTable;
        }


        public DataTable GetWorkStrengh()
        {
            DBService DBS = new DBService();
            DataTable dataTable = new DataTable();
            string Table_Work_Strengh =
                $@"  SELECT case when substring(B.DPPRF,0,6)='Mengi' then 'Maintenance engineering' else substring(B.DPPRF,0,9) end agaf , T.MAHLAKA as Unit ,B.CDESC UnitName,count(*) as Count
                     FROM isufkv.isav as T  inner join BPCSFV30.CDPL01 as B on right(trim(T.MAHLAKA),3)= B.CDEPT
                     WHERE T.mifal = '01' and (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129') and substring(T.kpa,7,6 )= '000000' and T.mahlaka*1 not in (196,197,198)
                     GROUP By B.DPPRF, T.MAHLAKA ,B.CDESC ,B.CDEPT
                     ORDER BY  substring(B.DPPRF,0,9) asc, T.MAHLAKA asc ";
            //$@"SELECT distinct case  when T.AGAF=0001 then 'לשכת מנכל' when T.AGAF=0002 THEN 'אגף כספים' when T.AGAF=0003 then 'אבטחת איכות' when T.AGAF=0006 or T.AGAF=0009 then 'אגף תפעול'  when T.AGAF=0004 then 'הנדסה' when T.AGAF=0008 then 'מופ'  when T.AGAF=0005 then 'אגף רכש ולוגיסטיקה'  when T.AGAF=0000 then 'פנסיונרים' when T.AGAF=9000 then 'לא ידוע' else t.agaf end agaf ,            //   T.MAHLAKA as Unit ,B.CDESC UnitName,count(*) as count            //   FROM isufkv.isav as T  inner join BPCSFV30.CDPL01 as B on right(trim(T.MAHLAKA),2)= B.CDEPT            //   WHERE T.mifal = '01'            //   GROUP By T.AGAF, T.MAHLAKA ,B.CDESC ,B.CDEPT";
            dataTable = DBS.executeSelectQueryNoParam(Table_Work_Strengh);
            return dataTable;
        }

        /// <summary>
        /// טבלה עבור חישוב כמות עובדים עקיפים ישירים ומנהלים
        /// </summary>
        /// <returns></returns>
        public DataTable GetSubTotalofEmployeeType()
        {
            DBService DBS = new DBService();
            DataTable TableForEmployeeType = new DataTable();
            string Table_Work_Strengh=
                  $@"SELECT  substring(B.DPPRF,0,9)  AS AGAF,T.MAHLAKA as Unit , H.HOFIL3 AS Type,count(*) as count
                     FROM isufkv.isav as T  inner join BPCSFV30.CDPL01 as B on right(trim(T.MAHLAKA),3)= B.CDEPT
	                 Left Join hsali.hovl02 as H on T.OVED=H.HOOVD
                      WHERE T.mifal = '01' and (substring(T.OVED,3,5) < '26000'  or  substring(T.OVED,3,5) > '27129') and substring(T.kpa,7,6 )= '000000' and T.mahlaka*1 not in (196,197,198) and ( right(trim(T.MAHLAKA),2)=H.HOMAH or right(trim(T.MAHLAKA),3)=H.HOMAH)
                     GROUP BY substring(B.DPPRF,0,9),T.MAHLAKA,H.HOFIL3
                     ORDER BY substring(B.DPPRF,0,9) asc,T.MAHLAKA asc ,H.HOFIL3  asc";

            TableForEmployeeType = DBS.executeSelectQueryNoParam(Table_Work_Strengh);
            return TableForEmployeeType;
        }




  
        /// <summary>
        /// סופר כמות פנסיונרים עבור מצבת כ"א
        /// </summary>
        /// <returns></returns>
        public int CountPensioner()
        {
        
            DBService DBS = new DBService();
            DataTable TableForCount = new DataTable();
            int count = 0;
            string CountString =
               $@"SELECT count(*)
                 FROM isufkv.isav as T
                 WHERE T.mifal = '01' and(substring(T.OVED, 3, 5) < '26000'  or  substring(T.OVED, 3, 5) > '27129') and substring(T.kpa,7,6 )= '000000' and T.mahlaka * 1 not in (196, 197, 198) and t.agaf = '0000'";
            TableForCount = DBS.executeSelectQueryNoParam(CountString);
            count = int.Parse(TableForCount.Rows[0][0].ToString());
            return count;
        }

        public int CountNonPayment()
        {
            DBService DBS = new DBService();
            DataTable TableForCount = new DataTable();
            int count = 0;
            string CountString =
              $@"SELECT count(*)
                FROM isufkv.isav as T
                WHERE T.mifal = '01' and(substring(T.OVED, 3, 5) < '26000'  or  substring(T.OVED, 3, 5) > '27129') and substring(T.kpa,7,6 )= '000000' and T.mahlaka * 1  in (196, 197, 198)";
            TableForCount = DBS.executeSelectQueryNoParam(CountString);
            count = int.Parse(TableForCount.Rows[0][0].ToString());
            return count;
        }
    }
}
