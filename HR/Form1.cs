﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;



namespace HR
{
    public partial class Form1 : Form
    {
        DBService DBS = new DBService();
        DataTable dataTable = new DataTable();
        bool FilterDate = false;//עבור סינון תאריך
        List<int> ListTotalIndex = new List<int>();//רשימה של איפה נמצא המילה total
        int Middle_Index = 0;//מיזוג ומרכוז עבור מצבת כ"א
        int tab ;//בשביל לדעת איזה אקסל לפתוח
        public Form1()
        {
            InitializeComponent();

            ShowEmployees();
            StartScreen();
            tabControl1.Selecting += new TabControlCancelEventHandler(tabControl1_Selecting);

        }


        private void StartScreen()
        {
            cbo_search.Items.Add("מספר עובד");
            cbo_search.Items.Add("שם");
            cbo_search.Items.Add("ת.ז");
            cbo_search.Items.Add("מחלקה");

        }

        /// <summary>
        /// הצגת כל העובדים או רק עובדים פעילים
        /// </summary>
        private void ShowEmployees()
        {

            Employee employee = new Employee(date_start1.Value, date_start2.Value);
            bool ShowAllEmployees;//בודק אם להציג גם עובדים לא פעילים
            if (cb_ShowAll.Checked == false)
            {
                ShowAllEmployees = false;
                dataTable = employee.GetData(ShowAllEmployees, FilterDate);
            }
            else
            {
                ShowAllEmployees = true;
                dataTable = employee.GetData(ShowAllEmployees, FilterDate);
            }
            FilterDate = false;
            InsertDataTable();

        }

        /// <summary>
        /// insert data table to data grid view
        /// </summary>
        /// <param name="dataTable"></param>
        private void InsertDataTable()
        {

            DataGridView_employees.DataSource = dataTable;
            count_total2.Text = dataTable.Rows.Count.ToString();
            dataTable.Columns.Add(new DataColumn("סוג עובד", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ותק", typeof(string)));
            dataTable.Columns.Add(new DataColumn("גיל", typeof(string)));
            dataTable.Columns.Add(new DataColumn("סוג הסכם", typeof(string)));
            

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {

                if (dataTable.Rows[i]["DateStart"].ToString() != "000000" && dataTable.Rows[i]["DateStart"].ToString() != "")
                {
                    var timeSpan = DateTime.Today - DateTime.Parse(dataTable.Rows[i]["DateStart"].ToString());
                    var years = timeSpan.Days / 365;
                    var months = (timeSpan.Days - years * 365) / 30;
                    //var days = timeSpan.Days - years * 365 - months * 30;
                    DataGridView_employees.Rows[i].Cells["ותק"].Value = years + "." + months;
                }
                if (dataTable.Rows[i]["BIRTHDAY"].ToString() != "000000" && dataTable.Rows[i]["BIRTHDAY"].ToString() != "")
                {
                    var timeSpan = DateTime.Today - DateTime.Parse(dataTable.Rows[i]["BIRTHDAY"].ToString());
                    var years = timeSpan.Days / 365;
                    DataGridView_employees.Rows[i].Cells["גיל"].Value = years;
                }

                if (dataTable.Rows[i]["NUMBER"].ToString().Substring(0, 2) == "19")
                    DataGridView_employees.Rows[i].Cells["סוג הסכם"].Value = "אישי";
                else DataGridView_employees.Rows[i].Cells["סוג הסכם"].Value = "קיבוצי";


                switch (int.Parse(DataGridView_employees.Rows[i].Cells["TypeTime"].Value.ToString())) //תיאור סוג עובד כתיאור ולא כמספר
                {
                    case 1:
                        DataGridView_employees.Rows[i].Cells["סוג עובד"].Value = "ישיר";
                        break;

                    case 2:
                        DataGridView_employees.Rows[i].Cells["סוג עובד"].Value = "עקיף";
                        break;

                    case 3:
                        DataGridView_employees.Rows[i].Cells["סוג עובד"].Value = "מנהל";
                        break;

                    case 4:
                        DataGridView_employees.Rows[i].Cells["סוג עובד"].Value = "עקיף חרושת";
                        break;

                    case 9:
                        DataGridView_employees.Rows[i].Cells["סוג עובד"].Value = "9";
                        break;

                    default:
                        break;
                }
            }
            //if(cb_ShowAll.Checked==false) dataTable.Columns.Remove("TypeTime");
            Change_Column_Header();

        }
        

        /// <summary>
        /// משנה כותרות עבור טב1 כ"א
        /// </summary>
        private void Change_Column_Header()
        {
            DataGridView_employees.Columns["NUMBER"].HeaderText = "מס עובד";
            DataGridView_employees.Columns["FirstName"].HeaderText = "שם פרטי";
            DataGridView_employees.Columns["LastName"].HeaderText = "שם משפחה";
            DataGridView_employees.Columns["Id"].HeaderText = "תעודת זהות";
            DataGridView_employees.Columns["ADRESS"].HeaderText = "כתובת";
            DataGridView_employees.Columns["DateStart"].HeaderText = "תאריך קליטה";
            DataGridView_employees.Columns["TypeTime"].HeaderText = "סוג";
            DataGridView_employees.Columns["Unit"].HeaderText = "מחלקה";
            DataGridView_employees.Columns["BIRTHDAY"].HeaderText = "תאריך לידה";
            DataGridView_employees.Columns["EFirstName"].HeaderText = "First Name";
            DataGridView_employees.Columns["ELastName"].HeaderText = "Last Name";
            DataGridView_employees.Columns["UNITNAME"].HeaderText = "שם מחלקה";
         



            if (cb_ShowAll.Checked)
                DataGridView_employees.Columns["Left"].HeaderText = "תאריך עזיבה";
        }


        /// <summary>
        /// משנה כותרות עבור מצבת כ"א
        /// </summary>
        private void Change_Column_Header2()
        {
            dataGrid_workStrengh.Columns["AGAF"].HeaderText = "שם אגף";
            dataGrid_workStrengh.Columns["UNIT"].HeaderText = "מספר מחלקה";
            dataGrid_workStrengh.Columns["COUNT"].HeaderText = "סך הכל";
            dataGrid_workStrengh.Columns["UNITNAME"].HeaderText = "שם מחלקה";
        }

        /// <summary>
        /// ממלא את הקומבו בוקס למטרת חיפוש
        /// </summary>
        private void Search_Func()
        {
            cbo_search_result.DataSource = dataTable;
            cbo_lastname.DataSource = dataTable;
            if (cbo_search.SelectedIndex == 0) { cbo_search_result.DisplayMember = "NUMBER"; cbo_lastname.Visible = false; }
            if (cbo_search.SelectedIndex == 1) { cbo_lastname.Visible = true; cbo_search_result.DisplayMember = "FirstName"; cbo_lastname.DisplayMember = "LastName"; }
            if (cbo_search.SelectedIndex == 2) { cbo_search_result.DisplayMember = "ID"; cbo_lastname.Visible = false; }
            if (cbo_search.SelectedIndex == 3) { cbo_search_result.DisplayMember = "UNIT"; cbo_lastname.Visible = false; }

        }



        private void initSelectTabs()
        {
            foreach (TabPage p in tabControl1.TabPages)
            {
                tabControl1.SelectedTab = p;
            }
            tabControl1.SelectedTab = tabpage_Employees;
            

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void cb_ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            ShowEmployees();
        }

        private void cbo_search_SelectedIndexChanged(object sender, EventArgs e)
        {
            Search_Func();
            btn_search.Enabled = true;
        }


        /// <summary>
        /// כפתור חיפוש לפי שדה ספציפי
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                Employee Employee_Result = new Employee();
                bool ShowAllEmployees;//בודק אם להציג גם עובדים לא פעילים
                if (cb_ShowAll.Checked == false)
                    ShowAllEmployees = false;
                else ShowAllEmployees = true;
                //int Select_Search_Field = cbo_search.SelectedIndex; //שם השדה על פיו מעוניינים לחפש
                //if (Select_Search_Field >= 0 && Select_Search_Field <= 3)
                dataTable = Employee_Result.TableAfterSearch(ShowAllEmployees, cbo_search.SelectedIndex, cbo_search_result.Text);

                InsertDataTable();
            }

            catch
            {
                MessageBox.Show("לא הקלדת ערך קיים");
                ShowEmployees();
            }


        }

        private void button1_Click_1(object sender, EventArgs e)///כפתור ביטול חיפוש
        {
            ShowEmployees();
        }

        private void DataGridView_employees_SortStringChanged(object sender, EventArgs e)
        {
            dataTable.DefaultView.Sort = this.DataGridView_employees.SortString;
            count_total2.Text = (DataGridView_employees.Rows.Count - 1).ToString();
        }





        private void btn_show_Click(object sender, EventArgs e)
        {
            try
            {
                FilterDate = true;
                ShowEmployees();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShowEmployees();
            }

        }

        private void DataGridView_employees_FilterStringChanged(object sender, EventArgs e)
        {
            dataTable.DefaultView.RowFilter = this.DataGridView_employees.FilterString;
            count_total2.Text = (DataGridView_employees.Rows.Count -1).ToString();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            initSelectTabs();

            //this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.Fixed3D; ;
            this.WindowState = FormWindowState.Maximized;
            dataGrid_workStrengh.AutoGenerateColumns = false;//קשור לאיחוד תאים במצבת כ"א

        }

        /// <summary>
        /// מעבר למצבת כ"א
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            TabPage current = (sender as TabControl).SelectedTab;
            tab = e.TabPageIndex;//לדעת איזה קובץ אקסל לפתוח
            if (e.TabPage == tabpage_Employees)//לטפללל
            {
                WorkStrengh();
            }

        }

        /// <summary>
        /// הצגת מצבת כ"א
        /// </summary>
        private void WorkStrengh()
        {
            Employee employee = new Employee();
            try
            {
                int last_column = 7;//עבור מיקום עמודת ספירה סופית
                DataTable MaindataTable = employee.GetWorkStrengh();//מקבל טבלת מצבת כ"א        
                MaindataTable.Columns.Add("עובד ישיר", typeof(int));
                MaindataTable.Columns.Add("עובד עקיף", typeof(int));
                MaindataTable.Columns.Add("מנהל", typeof(int));
                MaindataTable.Columns.Add("עקיף חרושת", typeof(int));



                ///חישובים עבור חישובי ביניים של עובדים ישירים/עקיפים/מנהלים 
                DataTable TableForEmployeeType = employee.GetSubTotalofEmployeeType();
                string department = TableForEmployeeType.Rows[0]["UNIT"].ToString();//  מקבל שם מחלקה מטבלת החישובים 
                int row_index = 0;//מיקום השורה בטבלה המקורית בה יושמו המספרים של עיקרי וישיר
                for (int k = 0; k < TableForEmployeeType.Rows.Count; k++)//טבלה מקורית מול טבלת החישובים
                {
                    if (!(MaindataTable.Rows[row_index]["UNIT"].ToString() == department))//עובר על רשומה בטבלה הראשית ובודק את המחלקה עם סוגי העובדים שלה, אם אין יותר עובר לשורה הבאה
                    {
                        row_index++;
                    }


                    switch (int.Parse(TableForEmployeeType.Rows[k]["TYPE"].ToString()))///הכנסה לשורה הרלוונטית
                    {
                        case 1:
                            MaindataTable.Rows[row_index]["עובד ישיר"] = TableForEmployeeType.Rows[k]["COUNT"].ToString();
                            break;

                        case 2:
                            MaindataTable.Rows[row_index]["עובד עקיף"] = TableForEmployeeType.Rows[k]["COUNT"].ToString();
                            break;

                        case 3:
                            MaindataTable.Rows[row_index]["מנהל"] = TableForEmployeeType.Rows[k]["COUNT"].ToString();
                            break;

                        case 4:
                            MaindataTable.Rows[row_index]["עקיף חרושת"] = TableForEmployeeType.Rows[k]["COUNT"].ToString();
                            break;

                        default:
                            break;


                    }
                    if (k + 1 != TableForEmployeeType.Rows.Count)
                        department = TableForEmployeeType.Rows[k + 1]["UNIT"].ToString();

                }

                ///יצירת הטבלה לפי אגפים עם סיכומי ביניים
                string division = MaindataTable.Rows[0]["AGAF"].ToString();//מקבל שם אגף
                int grand_total = 0, total_direct = 0, total_indirect = 0, total_manager = 0, total_indirect_indust = 0;

                ///עובר על כל השורות וסוכם כל טור לפי הסוג שלו
                for (int i = 0; i <= MaindataTable.Rows.Count; i++)//כל סיום שם אגף עושה סב טוטל
                {
                    if (i != MaindataTable.Rows.Count)
                    {
                        if (MaindataTable.Rows[i]["AGAF"].ToString() == division)
                        {
                            if (!string.IsNullOrEmpty(MaindataTable.Rows[i]["עובד ישיר"].ToString() as string)) total_direct += int.Parse(MaindataTable.Rows[i]["עובד ישיר"].ToString());
                            if (!string.IsNullOrEmpty(MaindataTable.Rows[i]["עובד עקיף"].ToString() as string)) total_indirect += int.Parse(MaindataTable.Rows[i]["עובד עקיף"].ToString());
                            if (!string.IsNullOrEmpty(MaindataTable.Rows[i]["מנהל"].ToString() as string)) total_manager += int.Parse(MaindataTable.Rows[i]["מנהל"].ToString());
                            if (!string.IsNullOrEmpty(MaindataTable.Rows[i]["עקיף חרושת"].ToString() as string)) total_indirect_indust += int.Parse(MaindataTable.Rows[i]["עקיף חרושת"].ToString());
                            grand_total += int.Parse(MaindataTable.Rows[i]["COUNT"].ToString());                              
                        }
                        else ///אם סיימנו לעבור על שם האגף ניצור שורה חדשה עם הסב טוטל שלו
                        {
                            CreateRow(MaindataTable, division, total_direct, total_indirect, total_manager, total_indirect_indust, grand_total, i);//יצירת שורה חדשה                            
                            division = MaindataTable.Rows[i + 1]["AGAF"].ToString();
                            grand_total = 0;
                            total_direct = 0;
                            total_indirect = 0;
                            total_manager = 0;
                            total_indirect_indust = 0;
                        }
                    }
                        else///שייך לסב טקסט האחרון,נועד בשביל לא לצאת מגבולות הטבלה
                        {
                            CreateRow(MaindataTable, division, total_direct, total_indirect, total_manager, total_indirect_indust, grand_total, i);//יצירת שורה חדשה                            
                        if (i + 1 < MaindataTable.Rows.Count)  ///כשהגענו לרשומה האחרונה בשביל לא ליצור שגיאה ולצאת מגבולות הטבלה
                            division = MaindataTable.Rows[i + 1]["AGAF"].ToString();
                        else
                        {
                            grand_total = 0;
                            total_direct = 0;
                            total_indirect = 0;
                            total_manager = 0;
                            total_indirect_indust = 0;
                            break;
                        }

                    }
         
                }

                //הכנסה לדטה גריד
                dataGrid_workStrengh.DataSource = MaindataTable;

                ///תצוגה
                dataGrid_workStrengh.Columns["AGAF"].Width = 150;
                dataGrid_workStrengh.Columns["UNITNAME"].Width = 150;
                dataGrid_workStrengh.Columns["UNIT"].Width = 80;
                dataGrid_workStrengh.Columns["עובד ישיר"].Width = 50;
                dataGrid_workStrengh.Columns["עובד עקיף"].Width = 50;
                dataGrid_workStrengh.Columns["מנהל"].Width = 50;
                dataGrid_workStrengh.Columns["עקיף חרושת"].Width = 50;
                dataGrid_workStrengh.Columns["COUNT"].Width = 100;
                dataGrid_workStrengh.Columns["COUNT"].DisplayIndex = last_column;

                //מבחין באיזה שורות קיימת המילה טוטל עבור עיצוב שורה
                foreach (DataGridViewRow rows in dataGrid_workStrengh.Rows)
                {
                    if (!string.IsNullOrEmpty(rows.Cells[0].Value as string))
                    {
                        int length = rows.Cells[0].Value.ToString().Length;
                        if (length >= 5)
                        {
                            if (rows.Cells[0].Value.ToString().Substring(0, 5) == "Total")
                            {
                                ListTotalIndex.Add(rows.Index);//מוסיף לרשימה את מספר השורה שנמצא טוטל
                            }
                            length = 0;
                        }
                    }
                }

                CalculateAllTotal(MaindataTable,total_direct, total_indirect, total_manager, total_indirect_indust, grand_total);//מחשב טוטל סופי
                MergeAndCenter();//שם במרכז את שם האגף
                Change_Column_Header2();//כותרות לדטה גריד
                CountPensionersAndNoPayment();//ספירת פנסיונרים וללא תשלום
            }



            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

     

        /// <summary>
        /// סופרת את כמות הפנסיונרים ומחלקה ללא תשלום
        /// </summary>
        private void CountPensionersAndNoPayment()
        {
            Employee employee = new Employee();
            int Counter_Pensioner=employee.CountPensioner();
            lbl_countOldEmployees.Text = Counter_Pensioner.ToString();
            int Counter_Nonpayment = employee.CountNonPayment();
            lbl_nonPayCount.Text = Counter_Nonpayment.ToString();
            lbl_nonPayCount.Text = Counter_Nonpayment.ToString();
        }




        /// <summary>
        /// create row for subtotal 
        /// </summary>
        private void CreateRow(DataTable MaindataTable,string division,int total_direct,int total_indirect,int total_manager,int total_indirect_indust,int grand_total,int i)
        {
            DataRow row = MaindataTable.NewRow();
            row[0] = "Total " + division;
            row["עובד ישיר"] = total_direct;
            row["עובד עקיף"] = total_indirect;
            row["מנהל"] = total_manager;
            row["עקיף חרושת"] = total_indirect_indust;
            row["COUNT"] = grand_total;
            MaindataTable.Rows.InsertAt(row, i);
        }


        /// <summary>
        /// חישוב כולל של כל הטבלה
        /// </summary>
        private void CalculateAllTotal(DataTable MaindataTable, int total_direct, int total_indirect, int total_manager, int total_indirect_indust, int grand_total)
        {
            DataRow row = MaindataTable.NewRow();
            for (int i = 0; i < ListTotalIndex.Count; i++)
            {
                int index = ListTotalIndex[i];
                total_direct += int.Parse(dataGrid_workStrengh.Rows[index].Cells["עובד ישיר"].Value.ToString());
                total_indirect += int.Parse(dataGrid_workStrengh.Rows[index].Cells["עובד עקיף"].Value.ToString());
                total_indirect_indust+= int.Parse(dataGrid_workStrengh.Rows[index].Cells["עקיף חרושת"].Value.ToString());
                total_manager+= int.Parse(dataGrid_workStrengh.Rows[index].Cells["מנהל"].Value.ToString());
                grand_total+= int.Parse(dataGrid_workStrengh.Rows[index].Cells["COUNT"].Value.ToString());
            }
            string division = "All";
            CreateRow(MaindataTable, division, total_direct, total_indirect, total_manager, total_indirect_indust, grand_total, MaindataTable.Rows.Count);
            total_direct = 0;total_indirect = 0;total_indirect_indust = 0;total_manager = 0;grand_total = 0;
        }

        /// <summary>
        /// בודק אם לאחד בין התאים-מכילים אותו ערך
        /// </summary>
        bool IsTheSameCellValue(int column, int row,string PreviousValue)
        {
            DataGridViewCell cell1 = dataGrid_workStrengh[column, row];
            //DataGridViewCell cell2 = dataGrid_workStrengh[column, row - 1];
            if (cell1.Value == null || PreviousValue == null)
            {
                return false;
            }
            return cell1.Value.ToString() == PreviousValue.ToString();
        }

        /// <summary>
        /// מוריד גבולות עבור כותרות האגפים
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_workStrengh_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            ///הורדת גבולות מכל הטבלה
            e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            e.AdvancedBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
            e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;

             ///אם מופיע שורה עם המילה טוטל תוחם גבולות ומעצב תאים
            if (ListTotalIndex.Contains(e.RowIndex))
            {
                e.AdvancedBorderStyle.Top = dataGrid_workStrengh.AdvancedCellBorderStyle.Top;
                e.AdvancedBorderStyle.Bottom = dataGrid_workStrengh.AdvancedCellBorderStyle.Bottom;
                e.CellStyle.Font= new Font("Tahoma", 12, FontStyle.Bold);
                e.CellStyle.BackColor = Color.DeepSkyBlue;
            }

            //עיצוב שורה אחרונה של טוטל כללי
            if(e.RowIndex== dataGrid_workStrengh.Rows.Count - 2)
            {
                e.AdvancedBorderStyle.Top = dataGrid_workStrengh.AdvancedCellBorderStyle.Top;
                e.AdvancedBorderStyle.Bottom = dataGrid_workStrengh.AdvancedCellBorderStyle.Bottom;
                e.CellStyle.Font = new Font("Tahoma", 20, FontStyle.Bold);
                e.CellStyle.BackColor = Color.Red;
                dataGrid_workStrengh.Rows[e.RowIndex].Height = 42;
            }
               
          

            //את טור שם האגף לא סוגר בגבולות
            if (e.RowIndex < 0 || e.ColumnIndex == 0)
                return;

            ///סגירה בגבולות של כל השאר
            else
            {
                if (!ListTotalIndex.Contains(e.RowIndex))
                {
                    e.AdvancedBorderStyle.Top = dataGrid_workStrengh.AdvancedCellBorderStyle.Top;
                    e.AdvancedBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.Inset;
                    e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.Single;
                }

            }


        }

        /// <summary>
        /// מאחד את התאים עם מילים שוות
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //    private void dataGrid_workStrengh_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        //    {
        //    try
        //    {
        //        //int r = MergeAndCenter(Middle_Index);
        //        if (e.RowIndex == 0)
        //            return;
        //        if (e.ColumnIndex == 0)
        //        {

        //            if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
        //            {
        //                switch (Middle_Index)
        //                {
        //                    case 0:
        //                        if (e.RowIndex != (ListTotalIndex[Middle_Index] / 2) - 1)
        //                        {
        //                            e.Value = "";
        //                            e.FormattingApplied = true;
        //                        }
        //                        else Middle_Index++;
        //                        break;

        //                    case 1:
        //                    case 2:
        //                    case 3:
        //                    case 4:

        //                        int num = ListTotalIndex[Middle_Index] - (ListTotalIndex[Middle_Index] - ListTotalIndex[Middle_Index - 1]) / 2;
        //                        if (e.RowIndex != num)
        //                        {
        //                            e.Value = "";
        //                            e.FormattingApplied = true;
        //                        }
        //                        else Middle_Index++;
        //                        break;
        //                }

        //            }

        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}


        ///ממרכז את שם האגף ע"י מחיקת שאר השורות
        private void MergeAndCenter()
        {
            try
            {
                for (int i = 1; i < dataGrid_workStrengh.Rows.Count -1; i++)
                {

                    ///אם מדובר בשורה שרשום בה טוטל המילה לא תימחק
                    bool RowWithTotal = false;
                    for (int j = 0; j < ListTotalIndex.Count; j++)
                    {
                        if (i == ListTotalIndex[j]) RowWithTotal = true;
                    }


                    if (!RowWithTotal) ///יכנס רק אם לא מדובר במילה טוטל
                    {
                        string SavePreviousValue = dataGrid_workStrengh.Rows[i].Cells[0].Value.ToString();//שומר ערך קודם למטרת השוואה מול תא נוכחי
                        if (IsTheSameCellValue(0, i, SavePreviousValue))
                        {
                            if (Middle_Index <= ListTotalIndex.Count)
                            {
                                if (Middle_Index == ListTotalIndex.Count) Middle_Index = 1000;///מחיקה במידה ומדובר ברגף אחרון

                                switch (Middle_Index)
                                {
                                    case 0:
                                        if (i != (ListTotalIndex[Middle_Index] / 2) - 1)
                                        {
                                            dataGrid_workStrengh.Rows[i].Cells[0].Value = "";
                                        }
                                        else  Middle_Index++;
                                        break;

                                    case 1000:
                                        if (i != (ListTotalIndex[ListTotalIndex.Count - 1] / 2) - 1)
                                        {
                                            dataGrid_workStrengh.Rows[i].Cells[0].Value = "";
                                        }
                                        break;

                                    default:
                                        int num, sub;
                                        sub = (ListTotalIndex[Middle_Index] - ListTotalIndex[Middle_Index - 1]) / 2;
                                        num = ListTotalIndex[Middle_Index] - sub;
                                        if (i != num)
                                        {
                                            dataGrid_workStrengh.Rows[i].Cells[0].Value = "";
                                        }
                                        else Middle_Index++;
                                        break;
                                }
                            }

                        }
                    }
                    }
                                    dataGrid_workStrengh.Rows[0].Cells[0].Value = "";
                                    Middle_Index = 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }



        }




    private void dataGrid_workStrengh_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(e.RowIndex.ToString());
        }

        private void dataGrid_workStrengh_Paint(object sender, PaintEventArgs e)
        {
            //DeleteFirstRow();
        
        }


        /// <summary>
        /// כפתור הדפסת מצבת כ"א
        /// </summary>
        Bitmap bmp;
        private void button3_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "דוח מצבת כא";//Header
            printer.SubTitle = string.Format("Date: {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.Footer = "HR";//Footer
            printer.FooterSpacing = 15;
            //Print landscape mode
            printer.printDocument.DefaultPageSettings.Landscape = true;

            dataGrid_workStrengh.Size = new Size(650, 180);
            //printPreviewDialog1.ShowDialog();


            //int height = dataGrid_workStrengh.Height;
            //dataGrid_workStrengh.Height = dataGrid_workStrengh.RowCount * dataGrid_workStrengh.RowTemplate.Height * 2;
            //dataGrid_workStrengh.Width = dataGrid_workStrengh.Width / 2;
            //bmp = new Bitmap(dataGrid_workStrengh.Width, dataGrid_workStrengh.Height);
            //dataGrid_workStrengh.DrawToBitmap(bmp, new Rectangle(0, 0, dataGrid_workStrengh.Width, dataGrid_workStrengh.Height));
            //dataGrid_workStrengh.Height = height;
            //printPreviewDialog1.ShowDialog();

            printer.PrintDataGridView(dataGrid_workStrengh); 

            dataGrid_workStrengh.Size = new Size(1615, 656);
            dataGrid_workStrengh.Anchor = (AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Top);
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            //e.Graphics.DrawImage(bmp, 0, 0);

        }


 



        /// <summary>
        ///  מצבת כ"א ייצוא לאקסל
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_excel_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Excel.Application xlexcel;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[3,1];//טווח מילוי הטבלה
            Excel.Range chartRange,divisionRange,agafRange;//קביעת רוחב עמודות
            chartRange = xlWorkSheet.get_Range("A1", "M1");
            divisionRange = xlWorkSheet.get_Range("F1");
            agafRange= xlWorkSheet.get_Range("H1");
            chartRange.ColumnWidth = 15;
            divisionRange.ColumnWidth = 35;
            agafRange.ColumnWidth = 30;

             
            //כותרות ועיצוב כללי
            //xlWorkSheet.get_Range("A1", "H1").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            //xlWorkSheet.get_Range("A1", "H1").Cells.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            xlWorkSheet.Cells[1, 1] = "מצבת כא מעודכן לתאריך " + DateTime.Today.ToShortDateString();
            xlWorkSheet.Cells[2, 1] = "סך הכל";
            xlWorkSheet.Cells[2, 2] = "עקיף חרושת";
            xlWorkSheet.Cells[2, 3] = "מנהל";
            xlWorkSheet.Cells[2, 4] = "עובד עקיף";
            xlWorkSheet.Cells[2, 5] = "עובד ישיר";
            xlWorkSheet.Cells[2, 6] = "שם מחלקה";
            xlWorkSheet.Cells[2, 7] = "מספר מחלקה";
            xlWorkSheet.Cells[2, 8] = "שם אגף";
            xlWorkSheet.get_Range("A1:H1").Merge();//מיזוג תאים
            xlWorkSheet.get_Range("A1:H1").Font.Bold = true;
            xlWorkSheet.get_Range("A1:H1").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGreen);
            xlWorkSheet.get_Range("A2:H2").RowHeight = 30;
            xlWorkSheet.get_Range("A1:H1").RowHeight = 50;
            xlWorkSheet.get_Range("A1:H1").Font.Size = 32;
            Excel.Range workSheet_range = xlWorkSheet.get_Range("A:M");
            workSheet_range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;//מרכוז טקסט
            workSheet_range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;//מרכוז טקסט
            xlWorkSheet.get_Range("H:H").Font.Bold = true;
            xlWorkSheet.get_Range("A2:H2").Font.Bold = true;
            xlWorkSheet.get_Range("A2:H2").Font.Size = 16;




            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

            Excel.Range UsedRange = xlWorkSheet.UsedRange;
            int lastUsedRow = UsedRange.Row + UsedRange.Rows.Count - 1;//שורה אחרונה עם טקסט באקסל
            chartRange = xlWorkSheet.get_Range("A1:H"+lastUsedRow);//עיצוב תאים
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            

            int i = 1;
            int FirstCellMerge = 3;//עבור מיזוג שמות אגפים
            for (i = 1; i < lastUsedRow; i++)
            {
                string rowCell="";
                if (!string.IsNullOrEmpty(xlWorkSheet.Cells[i, 8].Value2 as string))
                {
                    
                    rowCell = Convert.ToString(xlWorkSheet.Cells[i, 8].Value2);
                    if (rowCell.Substring(0, 3) == "Tot")//אם מתחיל בטוטל מעצב שורה
                    {
                        Excel.Range usedRange1 = xlWorkSheet.get_Range("A" + i + ":H" + i);
                        usedRange1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                        usedRange1.RowHeight = 27;
                        usedRange1.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlDouble;
                        usedRange1.Font.Bold = true;
                        xlWorkSheet.get_Range("H" + FirstCellMerge + ":H" + (i - 1)).Merge();
                        FirstCellMerge = i + 1;
                    }
                }
              
            }
            xlWorkSheet.get_Range("A" + lastUsedRow + ":H" + lastUsedRow).Font.Bold = true;
            xlWorkSheet.get_Range("A" + lastUsedRow + ":H" + lastUsedRow).Font.Size = 20;
            xlWorkSheet.get_Range("A" + lastUsedRow + ":H" + lastUsedRow).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.MediumVioletRed);


        }

        /// <summary>
        /// ייצוא אקסל כ"א
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            //כותרת
            Excel.Range First = xlWorkSheet.get_Range("A1:P1");
            First.Merge();//מיזוג תאים
            First.Font.Bold = true;
            First.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleTurquoise);
            First.RowHeight = 50;
            First.Font.Size = 32;
            xlWorkSheet.get_Range("A2:P2").Font.Bold = true;
            xlWorkSheet.get_Range("A2:P2").Font.Size = 16;
            //מרכוז
            Excel.Range workSheet_range = xlWorkSheet.get_Range("A:P");
            workSheet_range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;//מרכוז טקסט
            workSheet_range.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;//מרכוז טקסט
            workSheet_range.ColumnWidth = 12;
          

            Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[3, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            xlWorkSheet.Cells[1, 1] = "כוח אדם-אליאנס";
            xlWorkSheet.Cells[2, 16] = "מספר עובד";
            xlWorkSheet.Cells[2, 15] = "שם פרטי";
            xlWorkSheet.Cells[2, 14] = "שם משפחה";
            xlWorkSheet.Cells[2, 13] = "ת.ז";
            xlWorkSheet.Cells[2, 12] = "כתובת";
            xlWorkSheet.Cells[2, 11] = "תאריך לידה";
            xlWorkSheet.Cells[2, 10] = "סוג";
            xlWorkSheet.Cells[2, 9] = "מחלקה";
            xlWorkSheet.Cells[2, 8] = "תאריך לידה";
            xlWorkSheet.Cells[2, 7] = "First Name";
            xlWorkSheet.Cells[2, 6] = "Last Name";
            xlWorkSheet.Cells[2, 5] = "שם מחלקה";
            xlWorkSheet.Cells[2, 4] = "סוג עובד";
            xlWorkSheet.Cells[2, 3] = "ותק";
            xlWorkSheet.Cells[2, 2] = "גיל";
            xlWorkSheet.Cells[2, 1] = "סוג הסכם";

            //גבולות תא
            Excel.Range UsedRange, chartRange;
            UsedRange = xlWorkSheet.UsedRange;
            int lastUsedRow = UsedRange.Row + UsedRange.Rows.Count - 1;//שורה אחרונה עם טקסט באקסל
            chartRange = xlWorkSheet.get_Range("A1:P" + lastUsedRow);//עיצוב תאים
            chartRange.Borders.Color = System.Drawing.Color.Black.ToArgb();

        }

        /// <summary>
        /// שייך לאקסל מצבת כ"א
        /// </summary>
        private void copyAlltoClipboard()
        {
            DataObject dataObj=null;
            switch (tab)
            {
                case 0:
                    DataGridView_employees.SelectAll();
                    dataObj = DataGridView_employees.GetClipboardContent();
                    break;

                case 1:
                    dataGrid_workStrengh.SelectAll();
                    dataObj = dataGrid_workStrengh.GetClipboardContent();
                    break;

                default:
                    break;

            }

            
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }




        /// <summary>
        /// חסימת מיון מצבת כ"א
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_workStrengh_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewColumn column in dataGrid_workStrengh.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }


 

       
    }
    }


