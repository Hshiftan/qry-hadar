﻿namespace HR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabpage_Employees = new System.Windows.Forms.TabPage();
            this.btn_excel1 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btn_show = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.date_start1 = new System.Windows.Forms.DateTimePicker();
            this.date_start2 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.cbo_lastname = new System.Windows.Forms.ComboBox();
            this.cbo_search_result = new System.Windows.Forms.ComboBox();
            this.cb_ShowAll = new System.Windows.Forms.CheckBox();
            this.count_total2 = new System.Windows.Forms.Label();
            this.lbl_total = new System.Windows.Forms.Label();
            this.cbo_search = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.DataGridView_employees = new ADGV.AdvancedDataGridView();
            this.tabpage_WorkStrengh = new System.Windows.Forms.TabPage();
            this.lbl_countOldEmployees = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbl_nonPayCount = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btn_excel = new System.Windows.Forms.Button();
            this.lbl_printWorkStrenght = new System.Windows.Forms.Button();
            this.dataGrid_workStrengh = new System.Windows.Forms.DataGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cNoActivate = new System.Windows.Forms.CheckBox();
            this.cActivate = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_frequency = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txt_num_contract = new System.Windows.Forms.TextBox();
            this.lbl_num_contract = new System.Windows.Forms.Label();
            this.date_start = new System.Windows.Forms.DateTimePicker();
            this.txt_subject = new System.Windows.Forms.TextBox();
            this.datetime_over = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tabControl1.SuspendLayout();
            this.tabpage_Employees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_employees)).BeginInit();
            this.tabpage_WorkStrengh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_workStrengh)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabpage_Employees);
            this.tabControl1.Controls.Add(this.tabpage_WorkStrengh);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabControl1.Location = new System.Drawing.Point(37, 112);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1314, 698);
            this.tabControl1.TabIndex = 0;
            // 
            // tabpage_Employees
            // 
            this.tabpage_Employees.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabpage_Employees.Controls.Add(this.btn_excel1);
            this.tabpage_Employees.Controls.Add(this.label22);
            this.tabpage_Employees.Controls.Add(this.label21);
            this.tabpage_Employees.Controls.Add(this.label20);
            this.tabpage_Employees.Controls.Add(this.label19);
            this.tabpage_Employees.Controls.Add(this.label18);
            this.tabpage_Employees.Controls.Add(this.btn_show);
            this.tabpage_Employees.Controls.Add(this.label24);
            this.tabpage_Employees.Controls.Add(this.label13);
            this.tabpage_Employees.Controls.Add(this.date_start1);
            this.tabpage_Employees.Controls.Add(this.date_start2);
            this.tabpage_Employees.Controls.Add(this.button1);
            this.tabpage_Employees.Controls.Add(this.btn_search);
            this.tabpage_Employees.Controls.Add(this.cbo_lastname);
            this.tabpage_Employees.Controls.Add(this.cbo_search_result);
            this.tabpage_Employees.Controls.Add(this.cb_ShowAll);
            this.tabpage_Employees.Controls.Add(this.count_total2);
            this.tabpage_Employees.Controls.Add(this.lbl_total);
            this.tabpage_Employees.Controls.Add(this.cbo_search);
            this.tabpage_Employees.Controls.Add(this.label12);
            this.tabpage_Employees.Controls.Add(this.DataGridView_employees);
            this.tabpage_Employees.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabpage_Employees.Location = new System.Drawing.Point(4, 25);
            this.tabpage_Employees.Name = "tabpage_Employees";
            this.tabpage_Employees.Padding = new System.Windows.Forms.Padding(3);
            this.tabpage_Employees.Size = new System.Drawing.Size(1306, 669);
            this.tabpage_Employees.TabIndex = 0;
            this.tabpage_Employees.Text = "כ\"א";
            // 
            // btn_excel1
            // 
            this.btn_excel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_excel1.BackgroundImage = global::HR.Properties.Resources.Excel_Icon;
            this.btn_excel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_excel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btn_excel1.Location = new System.Drawing.Point(29, 58);
            this.btn_excel1.Name = "btn_excel1";
            this.btn_excel1.Size = new System.Drawing.Size(62, 45);
            this.btn_excel1.TabIndex = 371;
            this.btn_excel1.UseVisualStyleBackColor = false;
            this.btn_excel1.Click += new System.EventHandler(this.button2_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(-40, 180);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(69, 16);
            this.label22.TabIndex = 361;
            this.label22.Text = "לא מוגדר";
            this.label22.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(-40, 147);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 16);
            this.label21.TabIndex = 360;
            this.label21.Text = "פנסיונים";
            this.label21.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(-17, 83);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 16);
            this.label20.TabIndex = 359;
            this.label20.Text = "עקיף";
            this.label20.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(-17, 112);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 16);
            this.label19.TabIndex = 358;
            this.label19.Text = "מנהל";
            this.label19.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(-17, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 16);
            this.label18.TabIndex = 357;
            this.label18.Text = "ישיר";
            this.label18.Visible = false;
            // 
            // btn_show
            // 
            this.btn_show.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_show.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btn_show.Location = new System.Drawing.Point(478, 187);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(51, 23);
            this.btn_show.TabIndex = 356;
            this.btn_show.Text = "הצג";
            this.btn_show.UseVisualStyleBackColor = true;
            this.btn_show.Click += new System.EventHandler(this.btn_show_Click);
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label24.Location = new System.Drawing.Point(653, 187);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(174, 23);
            this.label24.TabIndex = 355;
            this.label24.Text = "עד תאריך קליטה";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label13.Location = new System.Drawing.Point(949, 187);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 23);
            this.label13.TabIndex = 354;
            this.label13.Text = "מתאריך קליטה";
            // 
            // date_start1
            // 
            this.date_start1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.date_start1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_start1.Location = new System.Drawing.Point(844, 188);
            this.date_start1.Name = "date_start1";
            this.date_start1.Size = new System.Drawing.Size(99, 22);
            this.date_start1.TabIndex = 353;
            // 
            // date_start2
            // 
            this.date_start2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.date_start2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_start2.Location = new System.Drawing.Point(549, 189);
            this.date_start2.Name = "date_start2";
            this.date_start2.Size = new System.Drawing.Size(98, 22);
            this.date_start2.TabIndex = 352;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(597, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 351;
            this.button1.Text = "בטל חיפוש";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btn_search
            // 
            this.btn_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_search.BackgroundImage = global::HR.Properties.Resources._800px_COLOURBOX3114347;
            this.btn_search.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_search.Enabled = false;
            this.btn_search.Location = new System.Drawing.Point(707, 60);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(44, 34);
            this.btn_search.TabIndex = 350;
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // cbo_lastname
            // 
            this.cbo_lastname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_lastname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_lastname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_lastname.FormattingEnabled = true;
            this.cbo_lastname.Location = new System.Drawing.Point(757, 69);
            this.cbo_lastname.Name = "cbo_lastname";
            this.cbo_lastname.Size = new System.Drawing.Size(98, 24);
            this.cbo_lastname.TabIndex = 349;
            this.cbo_lastname.Visible = false;
            // 
            // cbo_search_result
            // 
            this.cbo_search_result.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_search_result.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_search_result.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_search_result.FormattingEnabled = true;
            this.cbo_search_result.Location = new System.Drawing.Point(852, 69);
            this.cbo_search_result.Name = "cbo_search_result";
            this.cbo_search_result.Size = new System.Drawing.Size(98, 24);
            this.cbo_search_result.TabIndex = 348;
            // 
            // cb_ShowAll
            // 
            this.cb_ShowAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_ShowAll.AutoSize = true;
            this.cb_ShowAll.Font = new System.Drawing.Font("Tahoma", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.cb_ShowAll.Location = new System.Drawing.Point(1154, 187);
            this.cb_ShowAll.Name = "cb_ShowAll";
            this.cb_ShowAll.Size = new System.Drawing.Size(113, 27);
            this.cb_ShowAll.TabIndex = 347;
            this.cb_ShowAll.Text = "הצג הכל";
            this.cb_ShowAll.UseVisualStyleBackColor = true;
            this.cb_ShowAll.CheckedChanged += new System.EventHandler(this.cb_ShowAll_CheckedChanged);
            // 
            // count_total2
            // 
            this.count_total2.AutoSize = true;
            this.count_total2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.count_total2.ForeColor = System.Drawing.Color.Blue;
            this.count_total2.Location = new System.Drawing.Point(25, 18);
            this.count_total2.Name = "count_total2";
            this.count_total2.Size = new System.Drawing.Size(101, 37);
            this.count_total2.TabIndex = 344;
            this.count_total2.Text = "count";
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbl_total.ForeColor = System.Drawing.Color.Blue;
            this.lbl_total.Location = new System.Drawing.Point(122, 18);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(79, 37);
            this.lbl_total.TabIndex = 338;
            this.lbl_total.Text = "סה\"כ";
            // 
            // cbo_search
            // 
            this.cbo_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_search.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbo_search.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbo_search.FormattingEnabled = true;
            this.cbo_search.Location = new System.Drawing.Point(956, 69);
            this.cbo_search.Name = "cbo_search";
            this.cbo_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbo_search.Size = new System.Drawing.Size(121, 24);
            this.cbo_search.TabIndex = 319;
            this.cbo_search.SelectedIndexChanged += new System.EventHandler(this.cbo_search_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label12.Location = new System.Drawing.Point(1105, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 23);
            this.label12.TabIndex = 318;
            this.label12.Text = "חיפוש עובד לפי:";
            // 
            // DataGridView_employees
            // 
            this.DataGridView_employees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGridView_employees.AutoGenerateContextFilters = true;
            this.DataGridView_employees.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView_employees.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.DataGridView_employees.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView_employees.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridView_employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_employees.DateWithTime = false;
            this.DataGridView_employees.Location = new System.Drawing.Point(25, 220);
            this.DataGridView_employees.Name = "DataGridView_employees";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DataGridView_employees.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridView_employees.Size = new System.Drawing.Size(1242, 434);
            this.DataGridView_employees.TabIndex = 0;
            this.DataGridView_employees.TimeFilter = false;
            this.DataGridView_employees.SortStringChanged += new System.EventHandler(this.DataGridView_employees_SortStringChanged);
            this.DataGridView_employees.FilterStringChanged += new System.EventHandler(this.DataGridView_employees_FilterStringChanged);
            // 
            // tabpage_WorkStrengh
            // 
            this.tabpage_WorkStrengh.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabpage_WorkStrengh.Controls.Add(this.lbl_countOldEmployees);
            this.tabpage_WorkStrengh.Controls.Add(this.label25);
            this.tabpage_WorkStrengh.Controls.Add(this.lbl_nonPayCount);
            this.tabpage_WorkStrengh.Controls.Add(this.label23);
            this.tabpage_WorkStrengh.Controls.Add(this.btn_excel);
            this.tabpage_WorkStrengh.Controls.Add(this.lbl_printWorkStrenght);
            this.tabpage_WorkStrengh.Controls.Add(this.dataGrid_workStrengh);
            this.tabpage_WorkStrengh.Controls.Add(this.splitter1);
            this.tabpage_WorkStrengh.Location = new System.Drawing.Point(4, 25);
            this.tabpage_WorkStrengh.Name = "tabpage_WorkStrengh";
            this.tabpage_WorkStrengh.Padding = new System.Windows.Forms.Padding(3);
            this.tabpage_WorkStrengh.Size = new System.Drawing.Size(1306, 669);
            this.tabpage_WorkStrengh.TabIndex = 1;
            this.tabpage_WorkStrengh.Text = "מצבת כ\"א";
            // 
            // lbl_countOldEmployees
            // 
            this.lbl_countOldEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_countOldEmployees.AutoSize = true;
            this.lbl_countOldEmployees.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbl_countOldEmployees.ForeColor = System.Drawing.Color.Blue;
            this.lbl_countOldEmployees.Location = new System.Drawing.Point(795, 595);
            this.lbl_countOldEmployees.Name = "lbl_countOldEmployees";
            this.lbl_countOldEmployees.Size = new System.Drawing.Size(135, 45);
            this.lbl_countOldEmployees.TabIndex = 374;
            this.lbl_countOldEmployees.Text = "bbbbb";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(1007, 595);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(201, 45);
            this.label25.TabIndex = 373;
            this.label25.Text = "פנסיונרים";
            // 
            // lbl_nonPayCount
            // 
            this.lbl_nonPayCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_nonPayCount.AutoSize = true;
            this.lbl_nonPayCount.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbl_nonPayCount.ForeColor = System.Drawing.Color.Blue;
            this.lbl_nonPayCount.Location = new System.Drawing.Point(795, 537);
            this.lbl_nonPayCount.Name = "lbl_nonPayCount";
            this.lbl_nonPayCount.Size = new System.Drawing.Size(135, 45);
            this.lbl_nonPayCount.TabIndex = 372;
            this.lbl_nonPayCount.Text = "bbbbb";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(974, 537);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(234, 45);
            this.label23.TabIndex = 371;
            this.label23.Text = "ללא תשלום";
            // 
            // btn_excel
            // 
            this.btn_excel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_excel.BackgroundImage = global::HR.Properties.Resources.Excel_Icon;
            this.btn_excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_excel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btn_excel.Location = new System.Drawing.Point(7, 6);
            this.btn_excel.Name = "btn_excel";
            this.btn_excel.Size = new System.Drawing.Size(62, 45);
            this.btn_excel.TabIndex = 370;
            this.btn_excel.UseVisualStyleBackColor = false;
            this.btn_excel.Click += new System.EventHandler(this.btn_excel_Click);
            // 
            // lbl_printWorkStrenght
            // 
            this.lbl_printWorkStrenght.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbl_printWorkStrenght.BackgroundImage = global::HR.Properties.Resources.print_icon1;
            this.lbl_printWorkStrenght.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_printWorkStrenght.Location = new System.Drawing.Point(7, 57);
            this.lbl_printWorkStrenght.Name = "lbl_printWorkStrenght";
            this.lbl_printWorkStrenght.Size = new System.Drawing.Size(63, 46);
            this.lbl_printWorkStrenght.TabIndex = 369;
            this.lbl_printWorkStrenght.UseVisualStyleBackColor = false;
            this.lbl_printWorkStrenght.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGrid_workStrengh
            // 
            this.dataGrid_workStrengh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGrid_workStrengh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_workStrengh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGrid_workStrengh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_workStrengh.Location = new System.Drawing.Point(115, 70);
            this.dataGrid_workStrengh.Name = "dataGrid_workStrengh";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGrid_workStrengh.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGrid_workStrengh.Size = new System.Drawing.Size(1085, 464);
            this.dataGrid_workStrengh.TabIndex = 1;
            this.dataGrid_workStrengh.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_workStrengh_CellClick);
            this.dataGrid_workStrengh.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_workStrengh_CellDoubleClick);
            this.dataGrid_workStrengh.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGrid_workStrengh_CellPainting);
            this.dataGrid_workStrengh.Paint += new System.Windows.Forms.PaintEventHandler(this.dataGrid_workStrengh_Paint);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(3, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 663);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1306, 669);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ימי הולדת";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1306, 669);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "דו\"ח סבונים";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1306, 669);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "היעדרויות";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1306, 669);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "סיום עבודה";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Controls.Add(this.label37);
            this.tabPage7.Controls.Add(this.label11);
            this.tabPage7.Controls.Add(this.comboBox2);
            this.tabPage7.Controls.Add(this.label10);
            this.tabPage7.Controls.Add(this.cNoActivate);
            this.tabPage7.Controls.Add(this.cActivate);
            this.tabPage7.Controls.Add(this.label9);
            this.tabPage7.Controls.Add(this.textBox3);
            this.tabPage7.Controls.Add(this.label8);
            this.tabPage7.Controls.Add(this.comboBox1);
            this.tabPage7.Controls.Add(this.label4);
            this.tabPage7.Controls.Add(this.label1);
            this.tabPage7.Controls.Add(this.cbo_frequency);
            this.tabPage7.Controls.Add(this.textBox2);
            this.tabPage7.Controls.Add(this.textBox1);
            this.tabPage7.Controls.Add(this.txt_num_contract);
            this.tabPage7.Controls.Add(this.lbl_num_contract);
            this.tabPage7.Controls.Add(this.date_start);
            this.tabPage7.Controls.Add(this.txt_subject);
            this.tabPage7.Controls.Add(this.datetime_over);
            this.tabPage7.Controls.Add(this.label7);
            this.tabPage7.Controls.Add(this.label6);
            this.tabPage7.Controls.Add(this.label5);
            this.tabPage7.Controls.Add(this.label3);
            this.tabPage7.Controls.Add(this.label2);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1306, 669);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "פרטי עובד";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label38.Location = new System.Drawing.Point(693, 12);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(235, 58);
            this.label38.TabIndex = 353;
            this.label38.Text = "שם עובד";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label37.Location = new System.Drawing.Point(934, 12);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(296, 58);
            this.label37.TabIndex = 352;
            this.label37.Text = "-פרטי עובד";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label11.Location = new System.Drawing.Point(938, 410);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(281, 39);
            this.label11.TabIndex = 351;
            this.label11.Text = "פרטי התקשרות?";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(329, 283);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 350;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label10.Location = new System.Drawing.Point(468, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 23);
            this.label10.TabIndex = 349;
            this.label10.Text = "מחלקה";
            // 
            // cNoActivate
            // 
            this.cNoActivate.AutoSize = true;
            this.cNoActivate.Location = new System.Drawing.Point(179, 225);
            this.cNoActivate.Name = "cNoActivate";
            this.cNoActivate.Size = new System.Drawing.Size(77, 20);
            this.cNoActivate.TabIndex = 348;
            this.cNoActivate.Text = "לא פעיל";
            this.cNoActivate.UseVisualStyleBackColor = true;
            // 
            // cActivate
            // 
            this.cActivate.AutoSize = true;
            this.cActivate.Location = new System.Drawing.Point(286, 225);
            this.cActivate.Name = "cActivate";
            this.cActivate.Size = new System.Drawing.Size(56, 20);
            this.cActivate.TabIndex = 347;
            this.cActivate.Text = "פעיל";
            this.cActivate.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label9.Location = new System.Drawing.Point(419, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 23);
            this.label9.TabIndex = 346;
            this.label9.Text = "סטטוס עובד";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(286, 168);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(164, 23);
            this.textBox3.TabIndex = 345;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label8.Location = new System.Drawing.Point(503, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 23);
            this.label8.TabIndex = 344;
            this.label8.Text = "ותק";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(833, 703);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 343;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(1070, 703);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 23);
            this.label4.TabIndex = 342;
            this.label4.Text = "סטטוס";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(1052, 639);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 23);
            this.label1.TabIndex = 341;
            this.label1.Text = "סוג עובד";
            // 
            // cbo_frequency
            // 
            this.cbo_frequency.FormattingEnabled = true;
            this.cbo_frequency.Location = new System.Drawing.Point(834, 641);
            this.cbo_frequency.Name = "cbo_frequency";
            this.cbo_frequency.Size = new System.Drawing.Size(121, 24);
            this.cbo_frequency.TabIndex = 340;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(856, 173);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(164, 23);
            this.textBox2.TabIndex = 339;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(856, 121);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(164, 23);
            this.textBox1.TabIndex = 338;
            // 
            // txt_num_contract
            // 
            this.txt_num_contract.Location = new System.Drawing.Point(856, 288);
            this.txt_num_contract.Name = "txt_num_contract";
            this.txt_num_contract.Size = new System.Drawing.Size(164, 23);
            this.txt_num_contract.TabIndex = 331;
            // 
            // lbl_num_contract
            // 
            this.lbl_num_contract.AutoSize = true;
            this.lbl_num_contract.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbl_num_contract.Location = new System.Drawing.Point(1181, 288);
            this.lbl_num_contract.Name = "lbl_num_contract";
            this.lbl_num_contract.Size = new System.Drawing.Size(38, 23);
            this.lbl_num_contract.TabIndex = 330;
            this.lbl_num_contract.Text = "ת.ז";
            // 
            // date_start
            // 
            this.date_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.date_start.Location = new System.Drawing.Point(916, 348);
            this.date_start.Name = "date_start";
            this.date_start.Size = new System.Drawing.Size(104, 23);
            this.date_start.TabIndex = 329;
            // 
            // txt_subject
            // 
            this.txt_subject.Location = new System.Drawing.Point(856, 230);
            this.txt_subject.Name = "txt_subject";
            this.txt_subject.Size = new System.Drawing.Size(163, 23);
            this.txt_subject.TabIndex = 328;
            // 
            // datetime_over
            // 
            this.datetime_over.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_over.Location = new System.Drawing.Point(253, 116);
            this.datetime_over.Name = "datetime_over";
            this.datetime_over.Size = new System.Drawing.Size(104, 23);
            this.datetime_over.TabIndex = 322;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.Location = new System.Drawing.Point(415, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 23);
            this.label7.TabIndex = 321;
            this.label7.Text = "תאריך עזיבה";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(1078, 345);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 23);
            this.label6.TabIndex = 320;
            this.label6.Text = "תאריך קליטה";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(1125, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 23);
            this.label5.TabIndex = 319;
            this.label5.Text = "שם פרטי";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(1098, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 23);
            this.label3.TabIndex = 318;
            this.label3.Text = "שם משפחה";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(1101, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 23);
            this.label2.TabIndex = 317;
            this.label2.Text = "מספר עובד";
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImage = global::HR.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(1386, 822);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabpage_Employees.ResumeLayout(false);
            this.tabpage_Employees.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_employees)).EndInit();
            this.tabpage_WorkStrengh.ResumeLayout(false);
            this.tabpage_WorkStrengh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_workStrengh)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabpage_Employees;
        private System.Windows.Forms.TabPage tabpage_WorkStrengh;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ComboBox cbo_search;
        private System.Windows.Forms.Label label12;
        private ADGV.AdvancedDataGridView DataGridView_employees;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cNoActivate;
        private System.Windows.Forms.CheckBox cActivate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_frequency;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txt_num_contract;
        private System.Windows.Forms.Label lbl_num_contract;
        private System.Windows.Forms.DateTimePicker date_start;
        private System.Windows.Forms.TextBox txt_subject;
        private System.Windows.Forms.DateTimePicker datetime_over;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label count_total2;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox cb_ShowAll;
        private System.Windows.Forms.ComboBox cbo_search_result;
        private System.Windows.Forms.ComboBox cbo_lastname;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker date_start1;
        private System.Windows.Forms.DateTimePicker date_start2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGrid_workStrengh;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button lbl_printWorkStrenght;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button btn_excel;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbl_nonPayCount;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbl_countOldEmployees;
        private System.Windows.Forms.Button btn_excel1;
    }
}

